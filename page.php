<?php get_header();
  if( have_posts() ) {
    while( have_posts() ) {
      the_post();
      if( has_post_thumbnail() ) { ?>
        <div class="featured-image-container">
          <?php the_post_thumbnail('featured-image'); ?>

          <div class="container featured-image-content content-<?php the_field('position'); ?>">
            <div class="featured-image-content-container">
              <?php the_field('content'); ?>
            </div>
          </div>

        </div>
      <?php } ?>
      <div class="module">
        <div class="container">
          <div class="flexbox page-content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    <?php }
  }
get_footer(); ?>
