import './util/bootstrap';
import './util/my-functions';
import Router from './util/router';
import common from './routes/common';

const routes = new Router({
  common
});

jQuery(document).ready(() => routes.loadEvents());
