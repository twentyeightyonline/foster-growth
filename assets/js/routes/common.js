export default {
  init() {

    function menu(object) {
      if(object.hasClass('active')) {
        object.slideUp(300);
        object.removeClass('active');
      } else {
        object.slideDown(300, 0);
        object.addClass('active');
      }
    };

    $('.menu-button').on('click', function () {
      menu($('.mobile-menu-container'));
    });

    // Toggle menu button on/off

    // $('.menu-button').on('click', function () {
    //   $('.mobile-menu-container').toggleClass('active');
    // });

  },
  finalize() {

  }
}
