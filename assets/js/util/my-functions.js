export default {
  init() {

    function menu(object) {
      if(object.hasClass('active')) {
        object.slideUp(300);
        object.removeClass('active');
      } else {
        object.slideDown(300, 0);
        object.addClass('active');
      }
    };
  }
}
