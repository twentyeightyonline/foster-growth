<?php if( ! function_exists( 'te_theme_support' ) ) {
	function te_theme_support () {
		$supports = array(
			'post-thumbnails',
		);
		foreach ($supports as $support) {
			add_theme_support( $support );
		}
	}

	add_action( 'init', 'te_theme_support' );
}
