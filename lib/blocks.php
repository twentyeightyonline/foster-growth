<?php function te_register_acf_block_types() {

  acf_register_block_type(array(
      'name'              => 'infomatic',
      'title'             => __('Infomatic', 'fg'),
      'description'       => __('A custom Infomatic block.', 'fg'),
      'render_template'   => 'blocks/infomatic.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'edit',
      'keywords'          => array( 'recruiting', 'services', 'infomatic', 'twenty eighty' ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-infomatic', get_template_directory_uri() . '/build/blocks/infomatic.css' );
      },
  ));

  acf_register_block_type(array(
      'name'              => 'caption',
      'title'             => __('Caption', 'fg'),
      'description'       => __('A custom Caption block.', 'fg'),
      'render_template'   => 'blocks/caption.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'edit',
      'keywords'          => array( 'culture', 'values', 'twenty eighty' ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-caption', get_template_directory_uri() . '/build/blocks/caption.css' );
      },
  ));

  acf_register_block_type(array(
      'name'              => 'working-with-us',
      'title'             => __('Working With Us', 'fg'),
      'description'       => __('A custom Working With Us block.', 'fg'),
      'render_template'   => 'blocks/working-with-us.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'edit',
      'keywords'          => array( 'working', 'twenty eighty' ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-working-with-us', get_template_directory_uri() . '/build/blocks/working-with-us.css' );
      },
  ));

  acf_register_block_type(array(
      'name'              => 'how-we-help',
      'title'             => __('How We Help', 'fg'),
      'description'       => __('A custom How We Help block.', 'fg'),
      'render_template'   => 'blocks/how-we-help.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'edit',
      'keywords'          => array( 'how', 'we', 'help', 'twenty eighty' ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-how-we-help', get_template_directory_uri() . '/build/blocks/how-we-help.css' );
      },
  ));

  acf_register_block_type(array(
      'name'              => 'who-we-serve',
      'title'             => __('Who We Serve', 'fg'),
      'description'       => __('A custom Who We Serve block.', 'fg'),
      'render_template'   => 'blocks/who-we-serve.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'edit',
      'keywords'          => array( 'who', 'we', 'serve', 'twenty eighty' ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-who-we-serve', get_template_directory_uri() . '/build/blocks/who-we-serve.css' );
      },
  ));

  acf_register_block_type(array(
      'name'              => 'brands',
      'title'             => __('Brands', 'fg'),
      'description'       => __('A custom Brands block.', 'fg'),
      'render_template'   => 'blocks/brands.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'edit',
      'keywords'          => array( 'brands', 'twenty eighty' ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-brands', get_template_directory_uri() . '/build/blocks/brands.css' );
      },
  ));

  acf_register_block_type(array(
      'name'              => 'track-record',
      'title'             => __('Track Record', 'fg'),
      'description'       => __('A custom Track Record block.', 'fg'),
      'render_template'   => 'blocks/track-record.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'edit',
      'keywords'          => array( 'track', 'record', 'twenty eighty' ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-track-record', get_template_directory_uri() . '/build/blocks/track-record.css' );
      },
  ));

  acf_register_block_type(array(
      'name'              => 'contact',
      'title'             => __('Contact', 'fg'),
      'description'       => __('A custom Contact block.', 'fg'),
      'render_template'   => 'blocks/contact.php',
      'category'          => 'layout',
      'icon'              => 'format-image',
      'mode'              => 'preview',
      'keywords'          => array( 'contact', 'section', 'twenty eighty' ),
      'supports'          => array( 'mode' => false ),
      'enqueue_assets' => function(){
        wp_enqueue_style( 'fg-block-contact', get_template_directory_uri() . '/build/blocks/contact.css' );
      },
  ));

}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'te_register_acf_block_types');

}
