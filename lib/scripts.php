<?php add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'main', esc_html( get_stylesheet_directory_uri() ) . '/build/js/script-min.js', ['jquery'], 1, true );

	wp_enqueue_style( 'main', add_query_arg( array( 'v' => 1 ), get_stylesheet_uri() ) );
});
