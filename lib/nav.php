<?php if ( !function_exists( 'te_register_menus' ) ) {
	function te_register_menus () {
		// Nav Menus
		register_nav_menus(array(
		  'mobile' => __( 'Mobile Menu' ),
		  'desktop' => __( 'Desktop Menu' ),
		  'footer' => __( 'Footer Menu' ),
		));
	}
	add_action( 'init', 'te_register_menus' );
}
