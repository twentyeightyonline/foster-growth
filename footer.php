</main>
<footer>
  <div class="module footer">

    <nav class="footer-menu">
      <?php if ( has_nav_menu( 'footer' ) ) {
        $primary_args = array(
          'theme_location'  => 'footer',
          'menu_class'      => 'footer',
          'container'       => false,
        );
        wp_nav_menu( $primary_args );
      } ?>
    </nav>

    <div class="footer-logo">
      <a class="site-logo" href="<?php echo get_home_url(); ?>">
        <img class="logo" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/logo.png" alt="Logo">
      </a>
    </div>

    <div class="footer-company">
      <ul>
        <li>Enabling Growth through intelligent recruitment</li>
      </ul>
    </div>
  </div>

</footer>
<?php wp_footer(); ?>

  </body>
</html>
