<?php get_header();
  if( have_posts() ) {
    while( have_posts() ) {
      the_post();
      if( has_post_thumbnail() ) { ?>
        <div class="featured-image-container">
          <?php the_post_thumbnail('featured-image'); ?>
          <div class="featured-image-content content-<?php the_field('position'); ?>">
            <div class="featured-image-content-container">
              <div class="wrapper">
                <?php the_field('content'); ?>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
      <div class="module front-page-module">
        <div class="container">
          <div class="flexbox page-content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    <?php }
  }
get_footer(); ?>
