<?php

$id = 'fg-brands-' . $block['id'];
$config = (object) [ 'brands' => get_field( 'brand_items') ];

?>

<section class="brands">

  <?php foreach ($config->brands as $item) { ?>

    <div class="brand-item">

      <img src="<?php echo $item ['brand'] ['url'] ?>">

    </div>

  <?php } ?>

</section>
