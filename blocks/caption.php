<?php

$id = 'fg-caption-' . $block['id'];
$config = (object) [
  'caption' => get_field( 'caption_items') ,
  'icon_colour' => get_field ('icon_colour'),
  'columns' => get_field('number_of_columns')
];

?>

<section class="caption columns-<?php echo $config->columns ?>">

  <?php foreach ($config->caption as $item) { ?>

    <div class="caption-item">

      <div class="caption-item-container">
        <?php if( $config->icon_colour == 'light' ) { ?>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-single-white.png">
        <?php } else { ?>
          <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-single-black.png">
        <?php } ?>
        <div class="icon-container">
          <?php echo $item ['content'] ?>
        </div>
      </div>

    </div>

  <?php } ?>

</section>
