<?php  ?>

<section class="contact">
  <h1>Contact Us</h1>
  <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-single.png">
  <ul>
    <li><?php echo file_get_contents( get_template_directory() . '/assets/images/svg/phone.svg' ) ?><a href="tel:01344851300">01344 851 300</a></li>
    <li><?php echo file_get_contents( get_template_directory() . '/assets/images/svg/mobile.svg' ) ?><a href="tel:07811340704">07811 340 704</a></li>
    <li><?php echo file_get_contents( get_template_directory() . '/assets/images/svg/email.svg' ) ?><a href="mailto:james@fostergrowth.co.uk">james@fostergrowth.co.uk</a></li>
    <li><?php echo file_get_contents( get_template_directory() . '/assets/images/svg/home.svg' ) ?><a href="fostergrowth.co.uk">www.fostergrowth.co.uk</a></li>
    <li><a href="https://www.linkedin.com/company/foster-growth/"><?php echo file_get_contents( get_template_directory() . '/assets/images/svg/linkedin.svg' ) ?></a></li>
  </ul>
</section>
