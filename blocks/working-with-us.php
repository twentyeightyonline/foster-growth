<?php

$id = 'fg-working-with-us-' . $block['id'];
$config = (object) [ 'workingWithUs' => get_field( 'working_with_us_items') ]; ?>

<section class="working-with-us">

  <?php foreach ($config->workingWithUs as $item) { ?>

    <div class="working-with-us-item">

      <?php echo $item ['content'] ?>

      <a href="<?php echo get_permalink($item['button_page_link']); ?>" class="button"><?php echo $item['button_text'] ?></a>

    </div>

  <?php } ?>

</section>
