<?php

$id = 'fg-infomatic-' . $block['id'];
$config = (object) [ 'infomatic' => get_field( 'infomatic_items') ];

?>

<section class="infomatic">

  <?php foreach ($config->infomatic as $item) { ?>

    <div class="infomatic-item">

      <div class="infomatic-icon">

        <img class="background-image" src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/logo-single-black.png">

        <div class="icon-container">
          <img src="<?php echo $item ['icon'] ['url'] ?>">
        </div>

      </div>

      <div class="infomatic-content">
        <?php echo $item ['content'] ?>
      </div>

    </div>

  <?php } ?>

</section>
