<?php

$id = 'fg-wtrack-record-' . $block['id'];
$config = (object) [ 'trackRecord' => get_field( 'track_record_items') ];

?>

<section class="track-record">

  <?php foreach ($config->trackRecord as $item) { ?>

    <div class="track-record-item">

      <?php echo $item ['content'] ?>

    </div>

  <?php } ?>

</section>
