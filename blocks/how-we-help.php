<?php

$id = 'fg-how-we-help-' . $block['id'];
$config = (object) [ 'howWeHelp' => get_field( 'how_we_help_items') ];

?>

<section class="how-we-help">

  <?php foreach ($config->howWeHelp as $item) { ?>

    <div class="how-we-help-item">

      <?php echo $item ['content'] ?>

    </div>

  <?php } ?>

</section>
