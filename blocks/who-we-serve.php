<?php

$id = 'fg-who-we-serve-' . $block['id'];
$config = (object) [ 'whoWeServe' => get_field( 'who_we_serve_items') ];

?>
<section class="who-we-serve">

  <?php foreach ($config->whoWeServe as $item) { ?>

      <div class="who-we-serve-item">

        <div class="who-we-serve-item-top">
          <?php echo $item ['content_top'] ?>
        </div>

        <div class="who-we-serve-item-bottom">
          <?php echo $item ['content_bottom'] ?>
        </div>

      </div>

  <?php } ?>

</section>
