<?php $bg = "echo get_template_directory_uri()./assets/images/bg.jpg" ?>

<html <?php language_attributes(); ?>>
  <head>
    <meta name="theme-color" content="#111111">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://use.typekit.net/imi3mku.css">
    <?php wp_head(); ?>

  </head>

  <!-- Background Video -->

  <!-- <video class="bg" autoplay loop preload video muted src="https://immersify.co.uk/wp-content/uploads/2021/01/jellyfish.mp4"></video>
  <div class="bg-filter"></div> -->

  <body <?php body_class(); ?> >

    <!-- Mobile Menu -->
    <div class="module mobile-menu">
      <div class="container no-padding">
        <header class="site-nav">
          <div class="wrapper">
            <div class="site-logo-container">
              <a class="site-logo" href="<?php echo get_home_url(); ?>">
                <img class="logo" src="<?php echo get_stylesheet_directory_uri()?>/assets/images/logo.png" alt="Logo">
              </a>
            </div>
            <nav class="mobile-menu-container">
              <?php if ( has_nav_menu( 'mobile' ) ) {
                $primary_args = array(
                  'theme_location'  => 'mobile',
                  'menu_class'      => 'mobile-menu-wp',
                  'container'       => false,
                );
                wp_nav_menu( $primary_args );
              } ?>
            </nav>
            <div class="menu-button-container">
              <a class="menu-button"><?php echo file_get_contents( get_template_directory() . '/assets/images/svg/menu-button.svg' ) ?></a>
            </div>
          </div>
        </header>
      </div>
    </div>

    <!-- Desktop Menu -->
    <div class="module desktop-menu">
      <div class="container">
        <header class="site-nav">
          <div class="site-logo-container">
            <a class="site-logo" href="<?php echo get_home_url(); ?>">
              <img class="logo" src="<?php echo get_template_directory_uri()?>/assets/images/logo.png" alt="Logo">
            </a>
          </div>
          <div class="wrapper">

            <nav class="desktop-menu-container">
              <a class="site-logo" href="<?php echo get_home_url(); ?>">
                <?php echo file_get_contents( get_template_directory() . '/assets/images/svg/home.svg' ) ?>
              </a>
              <?php if ( has_nav_menu( 'desktop' ) ) {
                $primary_args = array(
                  'theme_location'  => 'desktop',
                  'menu_class'      => 'desktop-menu-wp',
                  'container'       => false,
                );
                wp_nav_menu( $primary_args );
              } ?>
            </nav>
          </div>
        </header>
      </div>
    </div>
    <main>
